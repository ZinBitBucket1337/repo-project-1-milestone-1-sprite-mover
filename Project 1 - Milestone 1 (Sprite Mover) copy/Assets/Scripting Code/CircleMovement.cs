﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMovement : MonoBehaviour {

    // Recognition that there is a Rigidbody2D with the game object.
    Rigidbody2D rb;
    // This allows the designer to modify the speed of the sprite.
    public float speed; 
      
    void Awake()
    {
        // Variable declaring that there is an attachment between the game object and the component of a Rigidbody2D.
        rb = GetComponent<Rigidbody2D>();
    }

    // This block of code allows for movement by allowing the user to control where they want to go by using the WASD buttons on the keyboard or the arrows on the keyboard.
    void Update()
    {
        //This line of code allows the user to move the sprite left or right.
        rb.AddForce(new Vector2(Input.GetAxis("Horizontal") * speed, 0));
        //This line of code allows the user to move the sprite down or up.
        rb.AddForce(new Vector2(0, Input.GetAxis("Vertical") * speed));
    }
}
